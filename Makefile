CXX=g++
OBJ = HuffmanTree.o SequenceCounter.o HuffmanTreeSerializer.o HuffmanPacker.o
TEMPLATE = src/BinaryTreeNode.h
FLAGS = -Wall -std=c++0x

huffman: $(OBJ)
	g++ -g src/main.cpp $(OBJ) $(TEMPLATE) $(FLAGS) -o bin/$@

clean: 
	rm -rf *.o *.back *.huf bin/*

%.o: src/%.cpp
	$(CXX) $(FLAGS) -c $^ -o $@
