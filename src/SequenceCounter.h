#ifndef SEQUENCE_COUNTER
#define SEQUENCE_COUNTER

#include <string>
#include <map>
#include <list>
#include <iostream>
#include <fstream>

using namespace std;

class SequenceCounter {
public:

    typedef map<char, int> SymbolMap;
    typedef multimap<int, char> FrequencyMap;
    typedef pair<int, char> FrequencyPair;
    typedef list<FrequencyPair> FrequencyList;

    SequenceCounter(ifstream* inFile);
    void Parse();
    FrequencyList GetFrequencies() const;
    void DumpValues() const;
private:
    SymbolMap byteFrequency;
    ifstream* sourceFile;
};

#endif
