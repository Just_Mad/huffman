#ifndef BINARY_TREE_NODE
#define BINARY_TREE_NODE
#include <cstddef>

template <typename T>
class BinaryTreeNode {
    T value;
    BinaryTreeNode *left, *right;

public:
    BinaryTreeNode(const T value);
    ~BinaryTreeNode();
    void AddLeft(const BinaryTreeNode* const newNode);
    void AddRight(const BinaryTreeNode* const newNode); 
    BinaryTreeNode<T>* GetLeft() const;
    BinaryTreeNode<T>* GetRight() const;
    T GetValue() const;
    void SetValue(T);
    bool isLeaf() const;
};

template <typename T>
T BinaryTreeNode<T>::GetValue() const {
    return this->value;
}

template <typename T>
void BinaryTreeNode<T>::SetValue(const T value) {
    this->value = value;
}

template <typename T>
BinaryTreeNode<T>::BinaryTreeNode(const T value) {
    this->value = value;
    left = NULL;
    right = NULL;
}

template <typename T>
void BinaryTreeNode<T>::AddLeft(const BinaryTreeNode* const newNode) {
    if (left != NULL) {
        delete left;
    }
    left = const_cast<BinaryTreeNode*>(newNode);
}

template <typename T>
void BinaryTreeNode<T>::AddRight(const BinaryTreeNode* const newNode) {
    if (right != NULL) {
        delete right;
    }
    right = const_cast<BinaryTreeNode*>(newNode);
}

template <typename T>
BinaryTreeNode<T>::~BinaryTreeNode() {
    delete left;
    delete right;
}

template <typename T>
BinaryTreeNode<T>* BinaryTreeNode<T>::GetLeft() const {
    return left;
}

template <typename T>
BinaryTreeNode<T>* BinaryTreeNode<T>::GetRight() const {
    return right;
}
template <typename T>
bool BinaryTreeNode<T>::isLeaf() const {
    return left == NULL && right == NULL;
}
#endif
