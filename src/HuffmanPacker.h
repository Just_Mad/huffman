#include <unistd.h>
#include "SequenceCounter.h"
#include "HuffmanTree.h"
#include "HuffmanTreeSerializer.h"

class HuffmanPacker {
    void packStream(ifstream* inFile, ofstream* outFile);
    void unpackStream(ifstream* inFile, ofstream* outFile);
public:
    void PackFile(string inFileName, string outFileName);
    void UnpackFile(string inFileName, string outFileName);
};
