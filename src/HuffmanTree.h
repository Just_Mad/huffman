#ifndef HUFFMAN_TREE
#define HUFFMAN_TREE
#include "BinaryTreeNode.h"
#include "SequenceCounter.h"

class HuffmanTree {
public: 
    typedef unsigned int CodeValue;
    typedef unsigned char CodeLength;
    typedef unsigned char CharCode;

    typedef BinaryTreeNode<SequenceCounter::FrequencyPair> FrequencyNode;
    typedef pair<CodeValue, CodeLength> SymbolCode;
    typedef map<CharCode, SymbolCode> SymbolCodes;

    HuffmanTree(const SequenceCounter::FrequencyList frequencies);
    HuffmanTree(FrequencyNode* node);
    FrequencyNode* GetTree();
    SymbolCode GetSymbolCode(CharCode symbol);
    ~HuffmanTree();
private:
    SymbolCodes* codes;
    FrequencyNode *top;
    FrequencyNode* joinNodes(const FrequencyNode *firstNode, const FrequencyNode *secondNode);
    list<FrequencyNode*>* transformToNodes(SequenceCounter::FrequencyList frequencies);
    void buildNextNode(list<FrequencyNode*>* nodes);
    void generateCodes(FrequencyNode* node, SymbolCodes* codes, int setBit, CodeValue prefix = 0, CodeLength level = 0);
};
#endif
