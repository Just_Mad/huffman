#include "SequenceCounter.h"

SequenceCounter::SequenceCounter(ifstream* inFile){
    sourceFile = inFile;
}

void SequenceCounter::Parse() {
    char symbol;
    sourceFile->seekg(0, ios::end);
    int fileSize = sourceFile->tellg();
    sourceFile->seekg(0, ios::beg);
    for (int i = 0; i < fileSize; i++) {
       sourceFile->read(&symbol, sizeof(char)); 
       int value = 0;
       if (byteFrequency.find(symbol) != byteFrequency.end()) {
           value = byteFrequency[symbol];
       }
       byteFrequency[symbol] = value + 1;
    }
}

SequenceCounter::FrequencyList SequenceCounter::GetFrequencies() const {
    multimap<int, char> valueOrderedMap;
    for (auto pair: byteFrequency) {
        valueOrderedMap.insert(FrequencyPair(pair.second, pair.first));
    }
    FrequencyList resultSequence;
    for (auto mapPair: valueOrderedMap) {
        resultSequence.push_back(pair<int, char>(mapPair.first, mapPair.second));
    }
    return resultSequence; 
}

void SequenceCounter::DumpValues() const {
    FrequencyList frequency = GetFrequencies();
    cout << "Total frequencies: " << frequency.size() << endl;
    for (auto pair: frequency) {
        cout << pair.first << " " << pair.second << endl;
    }
}
