#include "HuffmanTree.h"
#include <iostream>
#include <algorithm>
#include <iostream>

void dumpNodes(list<HuffmanTree::FrequencyNode*>* nodeList) {
    for_each(nodeList->begin(), nodeList->end(), [](HuffmanTree::FrequencyNode* node) {
        SequenceCounter::FrequencyPair value = node->GetValue();
        cout << value.second << ": " << value.first << endl;
    });
    cout << endl;
}

int countLeaves(HuffmanTree::FrequencyNode* node) {
    if (node == NULL) {
        return 0;
    }
    int total = node->isLeaf() ? 1 : 0;
    total += countLeaves(node->GetLeft());
    total += countLeaves(node->GetRight());
    return total;
}

void dumpTree(HuffmanTree::FrequencyNode* node, int level = 0) {
    if (node == NULL) {
        return;
    }
    for (int i = 0; i < level; i++) {
        cout << "-";
    }
    cout << node->GetValue().second << endl;
    dumpTree(node->GetLeft(), level + 1);
    dumpTree(node->GetRight(), level + 1);
}


void printBin(int value, int length) {
    for (int i = 0; i < length; i++) {
        int shiftValue = length - i - 1;
        int outBit = value & 1 << shiftValue;
        cout << (outBit >> shiftValue);
    }
}

void dumpCodes(HuffmanTree::SymbolCodes* codes) {
    for_each(codes->begin(), codes->end(), [](pair<char, HuffmanTree::SymbolCode> code) {
        HuffmanTree::SymbolCode sCode = code.second;
        int currentCode = sCode.first;
        cout << code.first << " ";
        printBin(currentCode, sCode.second);
        cout << endl;
    });
}

HuffmanTree::HuffmanTree(const SequenceCounter::FrequencyList frequencies) {
    codes = NULL;
    top = NULL;
    if (frequencies.size() == 0) {
        return;
    }

    list<FrequencyNode*>* nodes = transformToNodes(frequencies);
    //cout << "Size: " << nodes->size() << endl;
    //dumpNodes(nodes);

    do {
        buildNextNode(nodes);
        //dumpNodes(nodes);
    } while (nodes->size() > 1);
    top = nodes->front();
//    dumpTree(top);
    //cout << "Total leaves: " << countLeaves(top) << endl;
    codes = new SymbolCodes();
    generateCodes(top, codes, 0);
//    dumpCodes(codes);
    delete nodes;
}

HuffmanTree::HuffmanTree(FrequencyNode* root) {
    codes = NULL;
    top = root;
}

void HuffmanTree::generateCodes(FrequencyNode* node, SymbolCodes* codes, int setBit, CodeValue prefix, CodeLength level) {
    int newCode = (prefix << 1) | setBit;
    if (node->isLeaf()) {
        CharCode symbol = node->GetValue().second;
        codes->insert(pair<CharCode, SymbolCode>(symbol, SymbolCode(newCode, level)));
    } else {
        if (node->GetLeft() != NULL) {
            generateCodes(node->GetLeft(), codes, 0, newCode, level + 1);
        }
        if (node->GetRight() != NULL) {
            generateCodes(node->GetRight(), codes, 1, newCode, level + 1);
        }
    }
}

void HuffmanTree::buildNextNode(list<FrequencyNode*>* nodes) {
    FrequencyNode* first = nodes->front();
    nodes->pop_front();
    FrequencyNode* second = NULL;
    if (nodes->size() != 0) {
        second = nodes->front();
        nodes->pop_front();
    }
    FrequencyNode* parent = joinNodes(first, second);
    //dumpNodes(nodes);
    int parentValue = parent->GetValue().first;
    list<FrequencyNode*>::iterator found = find_if(nodes->begin(), nodes->end(), [=](FrequencyNode* node) {
        int nodeValue = node->GetValue().first;
        return parentValue <= nodeValue; 
    });
    if (found != nodes->end()) {
        nodes->insert(found, parent); 
    } else {
        nodes->push_back(parent);
    }
}

list<HuffmanTree::FrequencyNode*>* HuffmanTree::transformToNodes(const SequenceCounter::FrequencyList frequencies) {
    list<FrequencyNode*>* resultNodes = new list<FrequencyNode*>();
    resultNodes->resize(frequencies.size());
    transform(frequencies.begin(), frequencies.end(), resultNodes->begin(), [](const SequenceCounter::FrequencyPair frequency) {
        return new FrequencyNode(frequency);
    });
    return resultNodes;
}

HuffmanTree::FrequencyNode* HuffmanTree::joinNodes(const FrequencyNode *firstNode, const FrequencyNode *secondNode) {
    int frequencySum = 0;
    if (firstNode != NULL) {
        frequencySum += firstNode->GetValue().first;
    }
    if (secondNode != NULL) {
        frequencySum += secondNode->GetValue().first;
    }
    FrequencyNode* parentNode = new FrequencyNode(SequenceCounter::FrequencyPair(frequencySum, 0));
    parentNode->AddLeft(firstNode);
    parentNode->AddRight(secondNode);
    return parentNode;
}

HuffmanTree::FrequencyNode* HuffmanTree::GetTree() {
    return top;
}

HuffmanTree::SymbolCode HuffmanTree::GetSymbolCode(CharCode symbol) {
    return (*codes)[symbol];
}

HuffmanTree::~HuffmanTree() {
    delete top;
    delete codes;
}
