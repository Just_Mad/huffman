#include <string>
#include <fstream>
#include "HuffmanTree.h"

using namespace std;

class HuffmanTreeSerializer {
    enum SerializationTypes : char {
        NONE = 0,
        FOLDER = 1,
        LEAF = 2
    };
    void serializeTreeTo(ofstream* stream, HuffmanTree::FrequencyNode* node);
    void loadTreeFrom(ifstream* stream, HuffmanTree::FrequencyNode* node);

    void writeBinary(ofstream* stream, int value);
    void writeBinary(ofstream* stream, char value);

    HuffmanTree::FrequencyNode* tree;
public:
    HuffmanTreeSerializer(HuffmanTree::FrequencyNode* root = NULL);

    void Serialize(ofstream* outFile);
    HuffmanTree::FrequencyNode* Deserialize(ifstream* inFile);

    void PackFile(ifstream* inFile, ofstream* outFile, HuffmanTree* tree);
    void UnpackFile(ifstream* inFile, ofstream* outFile, HuffmanTree* tree);
};
