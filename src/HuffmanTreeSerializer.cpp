#include "HuffmanTreeSerializer.h"
#include <iostream>

void printBin1(int value, int length = 8) {
    for (int i = 0; i < length; i++) {
        int shiftValue = length - i - 1;
        int outBit = value & 1 << shiftValue;
        cout << (outBit >> shiftValue);
    } 
    cout << endl;
}

HuffmanTreeSerializer::HuffmanTreeSerializer(HuffmanTree::FrequencyNode* root) {
    tree = root;
}

void HuffmanTreeSerializer::Serialize(ofstream* outFile) {
    if (tree == NULL) {
        return;
    }
    serializeTreeTo(outFile, tree);
}

void HuffmanTreeSerializer::serializeTreeTo(ofstream* stream, HuffmanTree::FrequencyNode* node) {
    if (node == NULL) {
        return;
    }
    if (node->isLeaf()) {
        writeBinary(stream, static_cast<char>(SerializationTypes::LEAF));
        writeBinary(stream, node->GetValue().second);
    } else {
        writeBinary(stream, static_cast<char>(SerializationTypes::FOLDER));
        serializeTreeTo(stream, node->GetLeft());
        serializeTreeTo(stream, node->GetRight());
    }
}

HuffmanTree::FrequencyNode* HuffmanTreeSerializer::Deserialize(ifstream* inFile) {
    HuffmanTree::FrequencyNode* root = new HuffmanTree::FrequencyNode(SequenceCounter::FrequencyPair(0, 0));
    loadTreeFrom(inFile, root);
    return root;
}

void HuffmanTreeSerializer::loadTreeFrom(ifstream* stream, HuffmanTree::FrequencyNode* node) {
    SerializationTypes type;
    stream->read(reinterpret_cast<char*>(&type), sizeof(SerializationTypes));
    if (type == SerializationTypes::FOLDER) {
        node->AddLeft(new HuffmanTree::FrequencyNode(SequenceCounter::FrequencyPair(0, 0)));
        node->AddRight(new HuffmanTree::FrequencyNode(SequenceCounter::FrequencyPair(0, 0)));
        loadTreeFrom(stream, node->GetLeft());
        loadTreeFrom(stream, node->GetRight());
    } else {
        HuffmanTree::CharCode value;
        stream->read(reinterpret_cast<char*>(&value), sizeof(HuffmanTree::CharCode));
        node->SetValue(SequenceCounter::FrequencyPair(0, value));
    }
}

void HuffmanTreeSerializer::writeBinary(ofstream* stream, int value) {
    stream->write(reinterpret_cast<const char*>(&value), sizeof(int));
}

void HuffmanTreeSerializer::writeBinary(ofstream* stream, char value) {
    (*stream) << value;
}

void HuffmanTreeSerializer::PackFile(ifstream* inFile, ofstream* outFile, HuffmanTree* tree) {
    inFile->seekg(0, ios::end);
    int fileSize = inFile->tellg();
    inFile->seekg(0, ios::beg);
    writeBinary(outFile, fileSize);
    HuffmanTree::CharCode packedCode = 0;
    HuffmanTree::CodeLength currentPos = 0;
    bool byteFits = true;
    char size = sizeof(HuffmanTree::CharCode) * 8;

    HuffmanTree::CodeValue value = 0;
    HuffmanTree::CodeLength length;

    int posInFile = 0;
    int bytesWritten = 0;
    while (posInFile != fileSize || !byteFits) {
        if (byteFits) {
            HuffmanTree::CharCode symbol;
            inFile->read(reinterpret_cast<char*>(&symbol), sizeof(HuffmanTree::CharCode));
            HuffmanTree::SymbolCode symbolCode = tree->GetSymbolCode(symbol);
            length = symbolCode.second;
            value = symbolCode.first;
            byteFits = (currentPos + length) <= size;
            if (byteFits) {
                packedCode = packedCode | (value << (size - currentPos - length));
                currentPos = currentPos + length;
            } else {
                bytesWritten = size - currentPos;
                packedCode = packedCode | (value >> (length - bytesWritten));
                currentPos = size;
            }
            if (currentPos == size) {
                writeBinary(outFile, static_cast<char>(packedCode));
                currentPos = 0;
                packedCode = 0;
            }
            posInFile++;
        } else {
            int bytesLeft = length - bytesWritten;
            if (bytesLeft > size) {
                packedCode = value >> (bytesLeft - size);
                bytesWritten += size;
                currentPos = size;
            } else {
                packedCode = value << (size - bytesLeft);
                bytesWritten += bytesLeft;
                currentPos = bytesLeft;
            }
            byteFits = (bytesLeft < size);
            if (byteFits) {
                bytesWritten = 0;
            } 
            if (currentPos == size) {
                writeBinary(outFile, static_cast<char>(packedCode));
                currentPos = 0;
                packedCode = 0;
            }
        }
    }
    writeBinary(outFile, static_cast<char>(packedCode));
}

void HuffmanTreeSerializer::UnpackFile(ifstream* inFile, ofstream* outFile, HuffmanTree* tree) {
    int symbolCount = 0;
    int unpackedSymbols = 0;
    inFile->read(reinterpret_cast<char*>(&symbolCount), sizeof(symbolCount));
    HuffmanTree::FrequencyNode* root = tree->GetTree();
    HuffmanTree::FrequencyNode* current = root;
    while (unpackedSymbols < symbolCount) {
        char section;
        inFile->read(&section, sizeof(char));
        unsigned char checkBit = 128;
        while (checkBit != 0) {
            if (checkBit & section) {
                current = current->GetRight();
            } else {
                current = current->GetLeft();
            }
            if (current->isLeaf()) {
                writeBinary(outFile, current->GetValue().second);
                current = root;
                unpackedSymbols++;
                if (unpackedSymbols == symbolCount) {
                    return;
                }
            }
            checkBit = checkBit >> 1;
        }
    }
}
