#include <iostream>
#include "HuffmanPacker.h"

int main(int argc, char* argv[]) {
    if (argc < 3) {
        std::cout << "Not enough arguments" << std::endl;
        return -1;
    };

    string mode(argv[1]);
    string inFileName(argv[2]);
    string outFileName;

    const string COMPRESS = "-c";
    const string EXTRACT = "-x";
    const string DEFAULT_EXTENSION = ".huf";

    if (argc == 4) {
        outFileName = argv[3];
    } else if (mode == COMPRESS) {
        outFileName.append(inFileName).append(DEFAULT_EXTENSION);
    } else if (mode == EXTRACT) {
        int foundPosition = inFileName.find(DEFAULT_EXTENSION);
        int defaultExtensionPosition = inFileName.length() - DEFAULT_EXTENSION.length(); 
        if (foundPosition  == defaultExtensionPosition) {
            outFileName.append(inFileName);
            outFileName.replace(foundPosition, DEFAULT_EXTENSION.length(), "");
        } else {
            outFileName = "uncompressed";
        }
    } else {
        outFileName = "compressed";
    }

    HuffmanPacker packer;

    if (mode == COMPRESS) {
        packer.PackFile(inFileName, outFileName);
    } else if (mode == EXTRACT) {
        packer.UnpackFile(inFileName, outFileName);
    } else {
        std::cout << "Unknown option: " << argv[1] << std::endl;
    }
    return 0;
}
