#include "HuffmanPacker.h"
#include <fstream>
#include <iostream>

void HuffmanPacker::PackFile(string inFileName, string outFileName) {
    if (inFileName.empty() || outFileName.empty()) {
        return;
    }
    ifstream inFile(inFileName, ios::in | ios::binary); 
    ofstream outFile(outFileName, ios::out | ios::binary); 
    if (inFile.is_open() && outFile.is_open()) {
        std::cout << "Packing file \"" << inFileName << "\"..." << endl;
        packStream(&inFile, &outFile);
        std::cout << "Packing complete" << endl;
    } 
    inFile.close();
    outFile.close();
}

void HuffmanPacker::UnpackFile(string inFileName, string outFileName) {
    if (inFileName.empty() || outFileName.empty()) {
        return;
    }
    ifstream inFile(inFileName, ios::in | ios::binary); 
    ofstream outFile(outFileName, ios::out | ios::binary); 
    if (inFile.is_open() && outFile.is_open()) {
        std::cout << "Unpacking file \"" << inFileName << "\"..." << endl;
        unpackStream(&inFile, &outFile);
        std::cout << "Unpacking complete" << endl;
    }
    inFile.close();
    outFile.close();
}

void HuffmanPacker::packStream(ifstream* inFile, ofstream* outFile) {
    SequenceCounter counter(inFile);
    std::cout << "Counting frequencies..." << std::endl;
    counter.Parse();
    //counter.DumpValues();
    std::cout << "Building Huffman tree..." << std::endl;
    HuffmanTree tree(counter.GetFrequencies());
    HuffmanTreeSerializer serializer(tree.GetTree());
    std::cout << "Serializing Huffman tree..." << std::endl;
    serializer.Serialize(outFile);
    std::cout << "Packing file..." << std::endl;
    serializer.PackFile(inFile, outFile, &tree);
}

void HuffmanPacker::unpackStream(ifstream* inFile, ofstream* outFile) {
    HuffmanTreeSerializer serializer;
    std::cout << "Loading tree..." << std::endl;
    HuffmanTree tree(serializer.Deserialize(inFile));
    std::cout << "Unpacking file..." << std::endl;
    serializer.UnpackFile(inFile, outFile, &tree);
}
